# Android Automotive VoiceControl reference application

This is a sample voice assistant application that exemplifies how different voice control
applications can interact with the operating system to implement a set of core
voice-enabled experiences.

The application requires that the system has an implementation of a
[text-to-speech engine](https://developer.android.com/reference/android/speech/tts/TextToSpeechService)
 set as the system's default text-to-speech engine ([TTS_DEFAULT_SYNTH](https://developer.android.com/reference/android/provider/Settings.Secure#TTS_DEFAULT_SYNTH)).
